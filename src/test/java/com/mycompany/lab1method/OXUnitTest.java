/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.lab1method;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author informatics
 */
public class OXUnitTest {
    
    public OXUnitTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void testCheckWinCross1_O_output_true() {
        char[][] table = {{'O', '-', '-'},{'-', 'O', '-'},{'-', '-', 'O'}};
        char currentPlayer = 'O';
        boolean result = Lab1Method.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWinCross1_O_output_false() {
        char[][] table = {{'X', '-', '-'},{'-', 'O', '-'},{'-', '-', 'O'}};
        char currentPlayer = 'O';
        boolean result = Lab1Method.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWinCross1_X_output_true() {
        char[][] table = {{'X', '-', '-'},{'-', 'X', '-'},{'-', '-', 'X'}};
        char currentPlayer = 'X';
        boolean result = Lab1Method.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWinCross1_X_output_false() {
        char[][] table = {{'O', '-', '-'},{'-', 'O', '-'},{'-', '-', 'O'}};
        char currentPlayer = 'X';
        boolean result = Lab1Method.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWinRow1_X_output_true() {
        char[][] table = {{'X', 'X', 'X'},{'-', 'O', '-'},{'-', '-', 'O'}};
        char currentPlayer = 'X';
        boolean result = Lab1Method.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWinRow1_X_output_false() {
        char[][] table = {{'O', 'O', 'X'},{'-', 'O', '-'},{'-', '-', 'O'}};
        char currentPlayer = 'X';
        boolean result = Lab1Method.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWinRow2_X_output_true() {
        char[][] table = {{'O', 'X', 'X'},{'X', 'X', 'X'},{'-', '-', 'O'}};
        char currentPlayer = 'X';
        boolean result = Lab1Method.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWinRow2_X_output_false() {
        char[][] table = {{'O', 'X', 'X'},{'O', 'O', 'O'},{'-', '-', 'O'}};
        char currentPlayer = 'X';
        boolean result = Lab1Method.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    
     @Test
    public void testCheckWinRow3_X_output_true() {
        char[][] table = {{'O', 'O', 'O'},{'-', 'O', '-'},{'X', 'X', 'X'}};
        char currentPlayer = 'X';
        boolean result = Lab1Method.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWinRow3_X_output_false() {
        char[][] table = {{'O', 'O', 'O'},{'-', 'O', '-'},{'-', '-', 'O'}};
        char currentPlayer = 'X';
        boolean result = Lab1Method.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWinCol1_O_output_false() {
        char[][] table = {{'X', 'X', 'X'},{'-', 'O', '-'},{'-', '-', 'O'}};
        char currentPlayer = 'O';
        boolean result = Lab1Method.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWinCol1_O_output_true() {
        char[][] table = {{'O', 'X', 'X'},{'O', 'O', '-'},{'O', '-', 'O'}};
        char currentPlayer = 'O';
        boolean result = Lab1Method.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWinCol2_O_output_true() {
        char[][] table = {{'X', 'O', 'X'},{'O', 'O', '-'},{'O', 'O', 'X'}};
        char currentPlayer = 'O';
        boolean result = Lab1Method.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWinCol2_O_output_false() {
        char[][] table = {{'X', 'O', 'X'},{'O', 'X', '-'},{'O', 'O', 'X'}};
        char currentPlayer = 'O';
        boolean result = Lab1Method.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWinCol3_O_output_false() {
        char[][] table = {{'X', 'O', 'X'},{'O', 'X', '-'},{'O', 'O', 'X'}};
        char currentPlayer = 'O';
        boolean result = Lab1Method.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWinCol3_O_output_true() {
        char[][] table = {{'X', 'O', 'O'},{'O', 'X', 'O'},{'O', 'X', 'O'}};
        char currentPlayer = 'O';
        boolean result = Lab1Method.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWinCross2_O_output_true() {
        char[][] table = {{'X', 'X', 'O'},{'X', 'O', '-'},{'O', 'O', 'X'}};
        char currentPlayer = 'O';
        boolean result = Lab1Method.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWinCross2_O_output_false() {
        char[][] table = {{'X', 'X', 'X'},{'X', 'X', 'X'},{'X', 'X', 'X'}};
        char currentPlayer = 'O';
        boolean result = Lab1Method.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWinCross2_X_output_true() {
        char[][] table = {{'O', 'O', 'X'},{'O', 'X', 'O'},{'X', 'O', 'O'}};
        char currentPlayer = 'X';
        boolean result = Lab1Method.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWinCross2_X_output_false() {
        char[][] table = {{'X', 'X', 'O'},{'X', 'O', '-'},{'O', 'O', 'X'}};
        char currentPlayer = 'X';
        boolean result = Lab1Method.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckDraw_output_true() {
        int i = 9;
        boolean result = Lab1Method.checkDraw(i);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckDraw_output_false() {
        int i = 8;
        boolean result = Lab1Method.checkDraw(i);
        assertEquals(false, result);
    }
}
