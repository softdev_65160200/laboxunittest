/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab1method;
import java.util.Scanner;
/**
 *
 * @author informatics
 */
public class Lab1Method {

    
    
    public static boolean checkAlready(char[][] tic,int r, int c) {
        return tic[r][c] == 'X' || tic[r][c] == 'O';
    }
    
    static boolean checkWinRow(int row,char[][] tic, char player) {
        for (int c = 0; c < 3; c++) {
            if (tic[row][c] != player) {
                return false;
            }
        }
        return true;
    }

    static boolean checkWinCol(int col, char[][] tic, char player) {
        for (int r = 0; r < 3; r++) {
            if (tic[r][col] != player) {
                return false;
            }
        }
        return true;
    }

    static boolean checkCross1(char[][] tic, char player) {
        return tic[0][0] == player && tic[1][1] == player && tic[2][2] == player;
    }

    static boolean checkCross2(char[][] tic, char player) {
        return tic[0][2] == player && tic[1][1] == player && tic[2][0] == player;
    }
    
    
    static boolean checkWin(char[][] tic, char player) {
        for (int row = 0; row < 3; row++) {
            if (checkWinRow(row, tic, player)) return true;
        }
        for (int col = 0; col < 3; col++) {
            if (checkWinCol(col, tic, player)) return true;
        }
        if (checkCross1(tic, player)) return true;
        else if (checkCross2(tic, player)) return true;
        return false;
    }
    
    static boolean checkDraw(int turn) {
        return turn == 9;
    }
   
}